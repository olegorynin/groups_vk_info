import requests as r
import db

V = 5.21
TOKEN = 'dcba202edcba202edcba202ef8dccd363dddcbadcba202ebcd8e1fa5d81e7574d6829ba'
OWNER_ID = 85635407
# POST_ID = 3199
NUMBER_POSTS = 3
global_list = []


def generate_url_vk(method, params):
    gen_url = f'https://api.vk.com/method/{method}?v={V}&access_token={TOKEN}'
    for param_name in params.keys():
        gen_url += f'&{param_name}={params[param_name]}'
    return gen_url


def parse():
    wall_get = r.get(generate_url_vk('wall.get', {'count': NUMBER_POSTS, 'owner_id': OWNER_ID}))
    # """цикл поста"""
    for numb in range(NUMBER_POSTS):
        curent_post_id = wall_get.json()['response']['items'][numb]['id']
        wall_getComments = r.get(generate_url_vk('wall.getComments', {'post_id': curent_post_id, 'owner_id': OWNER_ID}))
        count_of_comments = wall_getComments.json()['response']['count']

        print(curent_post_id)
        if 'error' in wall_getComments.json():
            continue
        # """цикл комментариев и комментаторов"""
        for comment in range(6):
            if not 'attachments' in wall_getComments.json()['response']['items'][comment]:
                continue
            print(wall_getComments.json()['response']['items'][comment])
            if 'photo' in wall_getComments.json()['response']['items'][comment]['attachments'][0]:
                curent_picture_id = wall_getComments.json()['response']['items'][comment]['attachments'][0]['photo'][
                    'id']
                curent_commentator_id = wall_getComments.json()['response']['items'][comment]['from_id']

                global_list.append({'post_id': curent_post_id, 'commentator_id': curent_commentator_id,
                                    'picture_id': curent_picture_id})


class ID:
    def __init__(self, id_post, id_commentator, id_picture):
        self.id_post = id_post
        self.id_commentator = id_commentator
        self.id_picture = id_picture


def id_info(numb):
    return ID(global_list[numb]['post_id'], global_list[numb]['commentator_id'], global_list[numb]['picture_id'])


if __name__ == '__main__':
    parse()
    for i in range(len(global_list)):
        iD = id_info(i)
        db.save_comment(iD)
